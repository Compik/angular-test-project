import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PhoneNumberFieldModule } from '@pndc-ng/phone-number-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScaleFieldModule } from '@pndc-ng/scale-field';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    PhoneNumberFieldModule,
    ScaleFieldModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
