import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'angular-test-project';
  public testValue = 3;
  public formGroup: FormGroup = this.fb.group({
    phone: [],
    scale: [],
  });
  private destroyed$ = new EventEmitter();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    console.log('111', isNaN(Number('12')));
    console.log(typeof 13 === 'number');
    console.log('ALPHA', this.addAlpha('#428bca', 0.1));
    this.formGroup.controls.scale.setValue('');
    /*this.formGroup.controls.scale.disable();*/
    /*this.formGroup.controls.phone.setValue('74951231212');*/
    this.formGroup.valueChanges
      .pipe(takeUntil(this.destroyed$))
      .subscribe((value) => {
        console.log('value', value);
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  onChange(event: any): void {
    console.log('changed!!!!', event);
  }

  public addAlpha(color: string, alpha: number): string {
    const opacity = Math.round(Math.min(Math.max(alpha || 1, 0), 1) * 255);
    return color + opacity.toString(16).toUpperCase();
  }
}
